#include <stdio.h>

int calculate(int a, char operator, int b)
{
  switch (operator)
  {
  case '+':
    return a + b;
  case '-':
    return a - b;
  case '*':
    return a * b;
  case '/':
    if (b == 0)
    {
      return 0;
    }
    return a / b;
  default:
    return 0;
  }
}